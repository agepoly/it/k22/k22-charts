apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.instanceName }}-backend
  labels:
    app: {{ .Values.instanceName }}
    component: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{ .Values.instanceName }}
      component: backend
  template:
    metadata:
      labels:
        app: {{ .Values.instanceName }}
        component: backend
    spec:
      affinity:
        {{- if .Values.primaryBackend.enabled }}
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchFields:
                  - key: metadata.name
                    operator: In
                    values:
                      - turboflep
        {{- else}}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchLabels:
                  app: {{ .Values.instanceName }}
                  component: backend
              topologyKey: kubernetes.io/hostname
        {{- end}}

      containers:
        - name: backend
          image: registry.gitlab.com/agepoly/it/dev/agevote/agevote-backend:{{ ternary "primary" "secondary" .Values.primaryBackend.enabled }}-{{ .Values.images.backend }}
          imagePullPolicy: IfNotPresent

          env:
            - name: SIDECAR_SECRET
              valueFrom:
                secretKeyRef:
                  key: sidecar_secret
                  name: {{ .Values.secretName }}

            - name: SIDECAR_URL
              value: http://{{ .Values.instanceName }}-sidecar:9000

            - name: BACKEND_NAME
              value: {{ default .Values.instanceName .Values.backend.name }}

            - name: BACKENDS_URL
              value: {{ .Values.backend.allURLs }}

            - name: DKG_BEARER_TOKEN
              valueFrom:
                secretKeyRef:
                  key: dkg_bearer_token
                  name: {{ .Values.secretName }}

            - name: ROCKET_PORT
              value: "4000"

            - name: ROCKET_ADDRESS
              value: "0.0.0.0"

            {{- if .Values.primaryBackend.enabled }}
            - name: VOTING_GROUPS
              value: {{ .Values.primaryBackend.votingGroups }}
            
            - name: JWT_SECRET
              valueFrom:
                secretKeyRef:
                  key: jwt_secret
                  name: {{ .Values.secretName }}

            - name: BYPASS_AUTHORIZATION
              value: {{ default "false" .Values.primaryBackend.bypassAuthorization | quote }}

            - name: DATABASE_URL
              valueFrom:
                secretKeyRef:
                  key: database_url
                  name: {{ .Values.secretName }}

            - name: DEFAULT_OPERATOR
              value: {{ .Values.primaryBackend.defaultOperator | quote }}

            - name: TEQUILA_RETURN_URL
              value: {{ .Values.primaryBackend.tequilaReturnUrl }}

            - name: TICKETS_BASE_URL
              value: {{ .Values.primaryBackend.ticketsBaseUrl }}
            {{- end}}

          ports:
            - containerPort: 4000
              protocol: TCP
          
          resources: {}

          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /api
              port: 4000
              scheme: HTTP
            initialDelaySeconds: 20
            periodSeconds: 10
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /api
              port: 4000
              scheme: HTTP
            initialDelaySeconds: 20
            periodSeconds: 10
            timeoutSeconds: 10