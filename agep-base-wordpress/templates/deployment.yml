apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.instanceName }}
  labels:
    app: {{ .Values.instanceName }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{ .Values.instanceName }}
  template:
    metadata:
      labels:
        app: {{ .Values.instanceName }}
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchLabels:
                  app: {{ .Values.instanceName }}
              topologyKey: kubernetes.io/hostname

      containers:
      {{- if .Values.busyboxMode }}
        - image: busybox
          name: busybox
          volumeMounts:
          - mountPath: /mnt/bitnami-wordpress/
            name: bitnami-wordpress-v
          command: ["sleep", "99999"]
          resources: {}
      {{- else }}
        - name: wordpress
          image: bitnami/wordpress:6.3.1-debian-11-r35
          imagePullPolicy: IfNotPresent
          {{- if .Values.sleepEntrypoint }}
          command: ["sleep", "99999"]
          {{- end}}

          env:
            - name: WORDPRESS_DATABASE_HOST
              value: {{ .Values.databaseHost }}
            - name: WORDPRESS_DATABASE_NAME
              value: {{ .Values.databaseName }}
            
            - name: WORDPRESS_DATABASE_USER
              valueFrom:
                secretKeyRef:
                  key: databaseUsername
                  name: {{ .Values.secretName }}
            - name: WORDPRESS_DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: databasePassword
                  name: {{ .Values.secretName }}
            
            - name: WORDPRESS_SKIP_BOOTSTRAP
              value: "yes"
            - name: WORDPRESS_ENABLE_REVERSE_PROXY
              value: "yes"
            - name: WORDPRESS_ENABLE_HTTPS
              value: "no"
            - name: WORDPRESS_AUTO_UPDATE_LEVEL
              value: {{ default "none" .Values.wpAutoUpdateLevel | quote }}
            - name: WORDPRESS_DATA_TO_PERSIST
              value: {{ default "wp-config.php wp-content" .Values.wpDataToPersist | quote }}
          
          ports:
            - containerPort: 8080
              protocol: TCP
            - containerPort: 8443
              protocol: TCP
          
          resources: {}

          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 20
            periodSeconds: 10
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 20
            periodSeconds: 10
            timeoutSeconds: 10

          volumeMounts:
            - mountPath: /bitnami/wordpress/
              name: bitnami-wordpress-v
      {{- end }}
      {{- if .Values.vscode }}
        # https://github.com/linuxserver/docker-code-server
        - image: lscr.io/linuxserver/code-server:latest
          name: vscode
          volumeMounts:
            - mountPath: /mnt/wordpress/
              name: bitnami-wordpress-v
          env:
            - name: PASSWORD
              valueFrom:
                secretKeyRef:
                  key: vscodePassword
                  name: {{ .Values.secretName }}
            - name: SUDO_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: vscodePassword
                  name: {{ .Values.secretName }}
            - name: DEFAULT_WORKSPACE
              value: /mnt
            - name: PUID
              value: "1001"
            - name: PGID
              value: "1001"
            - name: PORT
              value: "30000"
          ports:
            - containerPort: 30000
              protocol: TCP
          resources: {}
      {{- end }}
      
      volumes:
        - name: bitnami-wordpress-v
          persistentVolumeClaim:
            claimName: {{ .Values.instanceName }}-bitnami-wordpress-pvc
