# K22 Helm Charts

```bash
# Add the Helm repository
helm repo add agepoly-k22 https://agepoly.gitlab.io/it/k22/k22-charts

# List the charts
helm search repo -l agepoly-k22
```